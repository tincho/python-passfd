Source: python-passfd
Maintainer: Martín Ferrari <tincho@debian.org>
Section: python
Priority: optional
Build-Depends: python-all-dev, debhelper (>= 9), dh-python
Standards-Version: 3.9.8
Homepage: https://github.com/TheTincho/python-passfd/
Vcs-Browser: https://anonscm.debian.org/cgit/users/tincho/python-passfd.git
Vcs-Git: https://anonscm.debian.org/git/users/tincho/python-passfd.git

Package: python-passfd
Architecture: any
Depends: ${misc:Depends}, ${python:Depends}, ${shlibs:Depends}
XB-Python-Version: ${python:Versions}
Provides: ${python:Provides}
Description: Python functions to pass file descriptors across UNIX domain
 This simple extension provides two functions to pass and receive file
 descriptors across UNIX domain sockets, using the BSD-4.3+ sendmsg() and
 recvmsg() interfaces.
 .
 Direct bindings to sendmsg and recvmsg are not provided, as the API does
 not map nicely into Python.
 .
 Please note that this only supports BSD-4.3+ style file descriptor
 passing, and was only tested on Linux.
